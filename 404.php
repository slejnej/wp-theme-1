<?php get_header() ?>

    <section class="white-bg">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h1>404 ERROR - PAGE NOT FOUND</h1>
                </div>
            </div>
            <div class="row margin-bottom">
                <div class="col-xs-12 col-md-6">
                    <p class="padding-top">
                        We’re really sorry, something went wrong. We can’t find that page.
                        It might have been moved or may have been an old link.
                    </p>
                    <p>
                        <a href="/">Visit the homepage</a>
                    </p>
                </div>
                <div class="col-xs-12 col-md-6">
                    <h3>Report website problem</h3>
                    <p>Let us know the page you were looking for and we'll fix the issue.</p>
                    <p>Send us an email <a href="mailto:js@jslejko.com">js@jslejko.com</a></p>
                </div>
            </div>
        </div>
    </section>

<?php get_footer() ?>