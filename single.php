<?php
/**
 * The template for displaying all single posts and attachments.
 */
get_header(); ?>

    <div id="primary" class="boxed-layout-header page-header header-small">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1 text-center">
                    <?php single_post_title( '<h1 class="hestia-title entry-title">', '</h1>' ); ?>
                    <h4 class="author">
                        <?php
                        echo apply_filters(
                            'hestia_single_post_meta', sprintf(
                                esc_html__( 'Published by %1$s on %2$s' ),
                                sprintf(
                                    '<a href="%2$s" class="vcard author"><strong class="fn">%1$s</strong></a>',
                                    esc_html( template_get_author( 'display_name' ) ),
                                    esc_url( get_author_posts_url( template_get_author( 'ID' ) ) )
                                ),
                                /* translators: %s is Date */
                                sprintf(
                                    '<time class="date updated published" datetime="%2$s">%1$s</time>',
                                    esc_html( get_the_time( get_option( 'date_format' ) ) ), esc_html( get_the_date( DATE_W3C ) )
                                )
                            )
                        );
                        ?>
                    </h4>
                </div>
            </div>
        </div>
        <?= attach_paralax(); ?>
    </div>
</header>
<div class="main main-raised">
    <div class="blog-post blog-post-wrapper">
        <div class="container">
            <?php
            if ( have_posts() ) :
                while ( have_posts() ) :
                    the_post();
                    get_template_part( 'includes/content', 'single' );
                endwhile;
            else :
                get_template_part( 'includes/content', 'none' );
            endif;
            ?>
        </div>
    </div>
</div>
<?php //do_action( 'hestia_blog_related_posts' ); ?>
<div class="footer-wrapper">
    <?php get_footer(); ?>
