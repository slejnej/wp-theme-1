/**
 * Created by jslejko on 19/07/17.
 */

jQuery(document).ready(function ($) {
    // remove user bio and wp profile picture
    if($('.user-description-wrap').length) {
        $('.user-description-wrap').closest('table').css('display', 'none');
    }
});