<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">

    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale = 1.0">

        <title><?php wp_title(); ?></title>

        <?php wp_head(); ?>
    </head>
    <body <?php body_class(); ?>>

    <div
        <?php
            if ( ! is_single() ) {
                echo 'class="wrapper"';
            } else {
                post_class( 'wrapper' );
            }
        ?>
    >
        <header class="header <?php echo esc_attr( $header_class ); ?>">
            <?php do_action( 'get_header' ); ?>
