<?php

define( 'INCLUDE_DIR', trailingslashit( get_template_directory() ) . 'includes/' );

update_option( 'thumbnail_size_w', 300 );
update_option( 'thumbnail_size_h', 180 );

add_action( 'admin_enqueue_scripts', function()
{
    wp_enqueue_script(
        'fwp-admin-js',
        sprintf('%s/js/wp-admin.js', get_template_directory_uri()),
        array('jquery'),
        time(),
        true
    );
});

add_action('wp_enqueue_scripts', function() {
    // we don't need JQuery Migrate
    wp_deregister_script( 'jquery' );
    wp_enqueue_script('jquery',
        sprintf('%s/assets/jquery/dist/jquery.min.js', get_template_directory_uri()),
        ['jquery'], 'v3.2.1', false
    );

    wp_enqueue_script('bootstrap',
        sprintf('%s/assets/bootstrap/dist/js/bootstrap.min.js', get_template_directory_uri()),
        ['jquery'], 'v3.3.7', false
    );
    wp_enqueue_script( 'jquery-material',
        sprintf('%s/js/material.js', get_template_directory_uri()),
        ['jquery'], 'v0.0.1', true
    );

    wp_enqueue_script('google-maps', sprintf('https://maps.google.com/maps/api/js?key=%s&callback=initMaps', GOOGLE_API_KEY),
        ['jquery'], false, true);

    wp_enqueue_style('custom-google-fonts', 'https://fonts.googleapis.com/css?family=Roboto:300,400,500,700', false );
    wp_enqueue_style('bootstrap', sprintf('%s/assets/bootstrap/dist/css/bootstrap.min.css', get_template_directory_uri()));
    wp_enqueue_style('font-awesome', sprintf('%s/assets/font-awesome/css/font-awesome.min.css', get_template_directory_uri()));
    wp_enqueue_style('template', sprintf('%s/css/wp-theme.css', get_template_directory_uri()));
}, 100);

add_action('acf/init',function() {
    acf_update_setting('google_api_key', GOOGLE_API_KEY);
});

require_once( INCLUDE_DIR . 'class-bootstrap-navwalker.php' );


/** Truncate text at nearest word after N number characters - default 100 */
function truncateText($string, $chars = 100)
{
    if (strlen($string) > $chars) {
        $string = substr($string, 0, $chars + 1);
        $pos = strrpos($string, ' ');
        $string = substr($string, 0, ($pos > 0)? $pos : $chars);
    }
    return $string."...";
}

function getAttatchmentMetadata($postId)
{
    if (!$postId) {
        return null;
    }

    global $wpdb;
    $result = $wpdb->get_var("SELECT meta_value FROM wp_postmeta WHERE post_id = $postId");
    return $result;
}

function attach_logo() {
    if ( the_field('custom_logo', 'option') ) {
        $image = wp_get_attachment_image_src( the_field('custom_logo', 'option'), 'full' );
        $alt_attribute = get_post_meta( the_field('custom_logo', 'option'), '_wp_attachment_image_alt', true );
        if ( empty( $alt_attribute ) ) {
            $alt_attribute = get_bloginfo( 'name' );
        }
        $logo = '<img src="' . esc_url( $image[0] ) . '" alt="' . esc_attr( $alt_attribute ) . '">';
    } else {
        $logo = '<p>' . get_bloginfo( 'name' ) . '</p>';
    }

    return $logo;
}

function attach_paralax() {
    if ( the_field('paralax_background', 'option') ) {
        $logo = wp_get_attachment_image_src( the_field('paralax_background', 'option'), 'full' );
        $paralax = '<div data-parallax="active" class="header-filter" style="background-image: url(' . esc_url( $logo[0] ) . '" ></div>';
    } else {
        $paralax = '';
    }

    return $paralax;
}

function template_header_background( $uses_default_header_image = true ) {
    if ( $uses_default_header_image == true ) {
        $background_image = get_header_image();
    } else {
        $currId = get_queried_object_id();
        $background_image = get_the_post_thumbnail_url($currId);
    } ?>

    <?php
    $customizer_background_image = get_background_image();

    $header_filter_div = '<div data-parallax="active" class="header-filter';

    /* Header Image */
    if ( ! empty( $background_image ) ) {
        $header_filter_div .= '" style="background-image: url(' . esc_url( $background_image ) . ');"';
        /* Gradient Color */
    } elseif ( empty( $customizer_background_image ) ) {
        $header_filter_div .= ' header-filter-gradient"';
        /* Background Image */
    } else {
        $header_filter_div .= '"';
    }
    $header_filter_div .= '></div>';

    echo apply_filters( 'header_wrapper_background_filter', $header_filter_div );
}

function template_get_category() {
    $category = get_the_category();
    if ( $category ) {
        /* translators: %s is Category name */
        echo '<a href="' . esc_url( get_category_link( $category[0]->term_id ) ) . '" title="' . esc_attr( sprintf( __( 'View all posts in %s' ), $category[0]->name ) ) . '" ' . '>' . esc_html( $category[0]->name ) . '</a> ';
    }
}

function template_get_author( $info ) {
    global $post;
    $author_id = $post->post_author;
    $author    = get_the_author_meta( $info, $author_id );

    return $author;
}

function do_theme_settings()
{
    // Enable global options for ACF
    if( function_exists('acf_add_options_page') ) {
        acf_add_options_page();
    }

    // Ensure thumbnail/featured image support
    if (!current_theme_supports('post-thumbnails')) {
        add_theme_support('post-thumbnails');
    }

    global $content_width;
    if ( ! isset( $content_width ) ) {
        $content_width = 750;
    }

    // Takes care of the <title> tag. https://codex.wordpress.org/Title_Tag
    if (!current_theme_supports('title-tag')) {
        add_theme_support('title-tag');
    }

    // Add theme support for custom logo. https://codex.wordpress.org/Theme_Logo
    add_theme_support(
        'custom-logo', array(
            'flex-width'  => true,
            'flex-height' => true,
            'height'      => 100
        )
    );

    // Add custom background support. https://codex.wordpress.org/Custom_Backgrounds
    add_theme_support(
        'custom-background', array(
            'default-color' => apply_filters( 'hestia_default_background_color', 'E5E5E5' )
        )
    );

    // Add custom header support. https://codex.wordpress.org/Custom_Headers
    $header_settings = apply_filters(
        'hestia_custom_header_settings', array(
            'height'      => 2000,
            'flex-height' => true,
            'header-text' => false
        )
    );
    add_theme_support( 'custom-header', $header_settings );

    add_theme_support( 'html5', array( 'search-form' ) );

    // This theme uses wp_nav_menu(). https://codex.wordpress.org/Function_Reference/register_nav_menu
    register_nav_menus(
        array(
            'primary' => esc_html__( 'Primary Menu' ),
            'footer' => esc_html__( 'Footer Menu' )
        )
    );
}
add_action( 'after_setup_theme', 'do_theme_settings' );

add_filter( 'body_class', function ( $classes ) {
    if ( is_singular() ) {
        $classes[] = 'blog-post';
    }

    return $classes;
});

add_filter( 'frontpage_template', function ( $template ) {
    return is_home() ? '' : $template;
} );

/** Allow html tags in descriptions. */
remove_filter( 'nav_menu_description', 'strip_tags' );

/** Call HEADER - nav. */
add_action( 'get_header', function () {
    $navbar_class = '';

    if ( get_option( 'show_on_front' ) === 'page' && is_front_page() && ! is_page_template() ) {
        $navbar_class = 'navbar-color-on-scroll navbar-transparent';
    }

    $navbar_class .= ' hestia_left';

    if ( ! is_home() && ! is_front_page() ) {
        $navbar_class .= ' navbar-not-transparent';
    }

    $navbar_class = apply_filters( 'hestia_header_classes', $navbar_class );

    ?>
    <nav class="navbar navbar-default navbar-fixed-top <?php echo esc_attr( $navbar_class ); ?>">
        <div class="container">
            <div class="navbar-header">
                <div class="title-logo-wrapper">
                    <a class="navbar-brand" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php bloginfo( 'name' ); ?>"><?php echo attach_logo(); ?></a>
                </div>
            </div>

            wp_nav_menu(
                array(
                    'theme_location'  => 'primary',
                    'container'       => 'div',
                    'container_class' => 'collapse navbar-collapse',
                    'container_id'    => 'main-navigation',
                    'menu_class'      => 'nav navbar-nav navbar-right',
                    'fallback_cb'     => 'bootstrap_navwalker::fallback',
                    'walker'          => new bootstrap_navwalker(),
                    'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                )
            );
            ?>
            <?php if ( has_nav_menu( 'primary' ) || current_user_can( 'manage_options' ) ) : ?>
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#main-navigation">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="sr-only"><?php esc_html_e( 'Toggle Navigation' ); ?></span>
                </button>
            <?php endif; ?>
        </div>

    </nav>
    <?php
});