<?php
/**
 * The front page template file.
 *
 * If the user has selected a static page for their homepage, this is what will appear.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 */


if ( ! is_page_template() ) {

		get_header();
		do_action( 'templ_header' ); ?>
	</header>
	<div class="main main-raised">

        <?php if( have_rows('features') ): ?>
            <?php get_template_part('includes/section_features', 'section-features'); ?>
        <?php endif; ?>

        <?php if( have_rows('testimonials') ): ?>
            <?php get_template_part('includes/section_testimonials', 'section-testimonials'); ?>
        <?php endif; ?>

        <?php if( have_rows('subscribe') ): ?>
            <?php get_template_part('includes/section_subscribe', 'section-subscribe'); ?>
        <?php endif; ?>

<?php
        get_footer();

} else {
	include( get_page_template() );
} ?>
