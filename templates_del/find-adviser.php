<?php
/**
 * Template Name: Find an Adviser
 */
?>

<?php
get_header();

$currId = get_queried_object_id();
$backDropImage = get_the_post_thumbnail_url($currId);
$postcode = $_POST['postcode'] ?: '';
?>
<script>window.sosProxyApp = '<?= SOS_PROXY_URL; ?>';</script>
<script type="text/javascript" src="/wp-content/themes/partner-wp-theme/assets/select2/dist/js/select2.min.js"></script>

<div class="block-image screen-height" style="background-image: url('<?= $backDropImage ?>')"></div>

<section>
    <div class="container">
        <div class="row margin-top margin-min-bottom text-center">
            <div class="col-xs-12 col-md-offset-1 col-md-10">
                <h2 class="indigo-900 margin-min-bottom"><?php echo get_field('findadviser_title'); ?></h2>
            </div>
            <div class="col-xs-12 col-md-offset-1 col-md-10">
                <p class="indigo-900">
                    <?php echo get_field('findadviser_subtitle'); ?>
                </p>
            </div>
        </div>
    </div>
    <div class="container indigo-900-bg-opq-80 padding-top padding-left padding-right padding-min-bottom">
        <form name="find-adviser" id="find-adviser">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="control-label white" for="find-adviser-firstName">Your Name<sup>*</sup></label>
                        <div>
                            <input type="text" class="form-control" id="find-adviser-firstName" name="firstName" required>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="control-label white" for="find-adviser-lastName">Your Surname<sup>*</sup></label>
                        <div>
                            <input type="text" class="form-control" id="find-adviser-lastName" name="lastName" required>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="control-label white" for="find-adviser-emailAddress">Email Address<sup>*</sup></label>
                        <div>
                            <input type="email" class="form-control" id="find-adviser-emailAddress" name="emailAddress" required>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="control-label white" for="find-adviser-phoneNumber">Phone Number<sup>*</sup></label>
                        <div>
                            <input type="number" class="form-control phone" id="find-adviser-phoneNumber" name="phoneNumber" required>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="control-label white" for="find-adviser-postcode">Post Code<sup>*</sup></label>
                        <div>
                            <input type="hidden" id="find-adviser-postcode-preset" value="<?= $postcode; ?>">
                            <select class="form-control" id="find-adviser-postcode" name="postcode" required></select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <hr style="border-top: 1px dotted white;"/>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="control-label white" for="find-adviser-advice">Area of advice sought</label>
                        <div>
                            <select class="form-control" id="find-adviser-advice" name="advice">
                                <option value=""></option>
                                <option value="Pensions and Retirement">Pensions and Retirement</option>
                                <option value="General Financial Advice">General Financial Advice</option>
                                <option value="Inheritance Tax">Inheritance Tax</option>
                                <option value="Long term care planning">Long term care planning</option>
                                <option value="Estate Management">Estate Management</option>
                                <option value="Equity Release">Equity Release</option>
                                <option value="Investments">Investments</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="control-label white" for="find-adviser-value">Approximate asset value</label>
                        <div>
                            <select class="form-control" id="find-adviser-value" name="value">
                                <option value="20000">under £20.000</option>
                                <option value="50000">£50.000+</option>
                                <option value="100000">£100.000+</option>
                                <option value="250000">£250.000+</option>
                                <option value="500000">£500.000+</option>
                                <option value="1000000">£1.000.000+</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="form-group">
                        <label class="control-label white" for="find-adviser-description">Message</label>
                        <div>
                            <textarea class="form-control full-width" name="description" id="find-adviser-description" rows="10"></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row margin-top">
                <div class="col-xs-12 col-md-8">
                    <span class="f-16 blue-500"><strong><sup>*</sup>Required information</strong></span>
                    <?php echo get_field('findadviser_privacy_policy'); ?>
                </div>
                <div class="col-xs-12 col-md-4 text-right">
                    <button class="btn indigo-a700-bg">
                        <i class="icon-graduation-cap"></i>
                        <?php echo get_field('findadviser_button'); ?>
                    </button>
                </div>
            </div>
            <input type="hidden" name="origin" id="find-adviser-origin" value="<?= $_COOKIE['origin']; ?>">
        </form>
    </div>
    <div class="container">
        <div class="row margin-top margin-bottom text-center">
            <div class="col-xs-12 col-md-offset-1 col-md-10">
                <h2 class="white margin-min-bottom"><?php echo get_field('findadviser_footer'); ?></h2>
            </div>
            <div class="col-xs-12 col-md-offset-1 col-md-10">
                <p class="white">
                    <?php echo get_field('findadviser_subfooter'); ?>
                </p>
            </div>
        </div>
    </div>
</section>

<?php get_footer(); ?>
