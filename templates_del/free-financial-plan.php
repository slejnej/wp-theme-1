<?php
/**
 * Template Name: Free Financial Plan
 */
?>

<?php get_header() ?>

<div class="block-image screen-height block-image-balloon" style="background-image: url('/wp-content/uploads/2017/07/balloon-mesh.jpg'); height: 800px;"></div>

<section id="home" class="screen-height">
    <div class="container">
        <div class="row content">
            <div class="col-xs-7">
                <h1 class="indigo-900">Get an instant personalised financial plan</h1>
                <h2 class="indigo-900">with helpful tips on how to plan for your future</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 text-center">
                <a class="btn orange-a700-bg f-36" href="//myfinancialplan.flyingcolourswealth.com">start your free plan</a>
            </div>
        </div>
        <div  class="scroll-to hidden-xs">
            <a href="#plan">
                <div class="scroll-down">
                    <span>
                        <i class="fa fa-angle-down fa-2x"></i>
                    </span>
                </div>
            </a>
        </div>
    </div>
</section>

<section class="white-bg padding-top">
    <div class="container text-center padding-bottom">
        <h2 class="indigo-900 text-left margin-min-bottom" id="plan">
            A financial plan can help...
        </h2>
        <div class="sm-up-row-equal-height">
            <div class="col-md-4 col-xs-12">
                <div class="image-box-item padding-min-top margin-min-top">
                    <h3 class="indigo-900"><strong>Grow your savings</strong></h3>
                    <img src="/wp-content/uploads/2017/07/money-plant.png" alt="" />
                </div>
            </div>
            <div class="col-md-4 col-xs-12">
                <div class="image-box-item padding-min-top margin-min-top  background-offset-1">
                    <h3 class="indigo-900"><strong>Reduce your tax bill</strong></h3>
                    <img src="/wp-content/uploads/2017/07/calculator.png" alt="" />
                </div>
            </div>
            <div class="col-md-4 col-xs-12">
                <div class="image-box-item padding-min-top margin-min-top background-offset-2">
                    <h3 class="indigo-900"><strong>Plan for the future</strong></h3>
                    <img src="/wp-content/uploads/2017/07/clock.png" alt="" />
                </div>
            </div>
        </div>
    </div>
</section>

<!--1,2,3 BOXES-->
<section class="white-bg padding-bottom">
    <div class="container text-center">
        <h2 class="indigo-900 text-left margin-min-bottom">
            How it works...
        </h2>
        <div class="sm-up-row-equal-height">
            <div class="col-md-4 col-xs-12">
                <div class="image-box-item padding-min-top margin-top padding-min-left padding-min-right">
                    <div class="circled indigo-900 indigo-900-border f-36">1</div>
                    <h3 class="indigo-900">Simply complete our short questionnaire</h3>
                </div>
            </div>
            <div class="col-md-4 col-xs-12">
                <div class="image-box-item padding-min-top margin-top background-offset-1 padding-min-left padding-min-right">
                    <div class="circled indigo-900 indigo-900-border f-36">2</div>
                    <h3 class="indigo-900">Answer a few questions about your financial goals</h3>
                </div>
            </div>
            <div class="col-md-4 col-xs-12">
                <div class="image-box-item padding-min-top margin-top background-offset-2 padding-min-left padding-min-right">
                    <div class="circled indigo-900 indigo-900-border f-36">3</div>
                    <h3 class="indigo-900">We'll send you an instant personalised financial report for FREE</h3>
                </div>
            </div>
        </div>
    </div>
</section>

<!--FEATURED IN BANNER-->
<section class="light-blue-900-bg-opq-60 white padding-top padding-bottom">
    <div class="container text-center">
        <div class="row">
            <div class="col-xs-12">
                <h2>As Featured In</h2>
                <div class="panel-body featured-in-body">
                    <img class="img-responsive center-block" src="/wp-content/uploads/2017/07/guardian-300x53.png" />
                    <img class="img-responsive center-block" src="/wp-content/uploads/2017/07/sunday_times-300x60.png" />
                    <img class="img-responsive center-block" src="/wp-content/uploads/2017/07/telegraph-300x48.png" />
                    <img class="img-responsive center-block" src="/wp-content/uploads/2017/07/bbc-300x117.png" />
                </div>
            </div>
        </div>
    </div>
</section>

<!--TESTIMONIALS-->
<section class="white-bg padding-top padding-bottom">
    <div class="container text-center">
        <h2 class="indigo-900">
            See what our clients have to say
        </h2>
        <div class="sm-up-row-equal-height">
            <div class="col-md-6 col-xs-12">
                <div class="drop-shadow testimonial-item margin-bottom padding-top">
                    <p>
                        <q class="comment f-28"><i>Having a plan helped me see how spending and saving today affects my finances.</i></q>
                    </p>
                    <div class="triangle-topleft"></div>
                </div>
            </div>
            <div class="col-md-6 col-xs-12">
                <div class="drop-shadow testimonial-item margin-bottom padding-top">
                    <p>
                        <q class="comment f-28"><i>Because of their advice I now have an investment that saves me a lot of tax.</i></q>
                    </p>
                    <div class="triangle-topleft"></div>
                </div>
            </div>
        </div>
        <a class="btn orange-a700-bg margin-top f-36" href="//ffp.flyingcolourswealth.com">start your free plan</a>
    </div>
</section>

<!--ABOUT US-->
<section class="white-bg">
    <div class="container padding-bottom">
        <div class="row">
            <div class="col-sm-12 padding-bottom">
                <h2 class="indigo-900">
                    About Flying Colours:
                </h2>
                <p>The team at Flying Colours is a mix of experienced financial advisers and investment experts. We are authorised and regulated by the Financial Conduct Authority.</p>
                <p>We believe that financial advice and investing should be affordable to all and not erode the very value you're trying to create or protect. We've designed our entire business with this approach in mind. We are proud to say that our clients seem to like what we're doing.</p>
                <a class="pull-right btn btn-more footer-element light-blue-50-bg" href="/our-service/lifetime-financial-planning/">See more <i class="fa fa-hand-o-right f-20"></i></a>
            </div>
        </div>
        <div class="row padding-top">
            <div class="col-sm-12 hidden-xs">
                <div class="image-container" style="background-image: url(/wp-content/themes/partner-wp-theme/images/FCLogo-Indigo900.svg);"/>
                <h3 class="title f-40 indigo-900">Outstanding Financial Advice</h3>
            </div>
        </div>
    </div>
</section>

<?php get_footer() ?>
