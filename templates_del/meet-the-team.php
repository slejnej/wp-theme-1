<?php
/**
 * Template Name: Meet the team
 */
?>

<?php
get_header();
$section = get_field('content_sections')[0];

$currId = get_queried_object_id();
$backDropImage = get_the_post_thumbnail_url($currId);
?>

<div class="block-image screen-height" style="background-image: url('<?= $backDropImage ?>')"></div>

<!--TEAM SELECTOR-->
<?php
    $uri_path = parse_url(ltrim($_SERVER['REQUEST_URI'], '/'), PHP_URL_PATH);
    $uri_segments = explode('/', $uri_path);
    $have_filters = false;
    if (is_page('who-we-are') && $uri_segments[0] === 'who-we-are') {
        $have_filters = true;
?>
    <section class="white-bg">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 text-center">
                    <div class="blue-50-bg padding-min-top padding-min-bottom">
                        <ul class="nav nav-tabs nav-justified" id="team-tabs">
                            <li class="active">
                                <a data-toggle="tab" class="btn" href="#all">
                                    All Team
                                </a>
                            </li>
                            <?php
                                foreach (get_field_object('user_location', 'user_15')['choices'] as $link => $title) {
                            ?>
                                <li class="">
                                    <a data-toggle="tab" class="btn" href="#" data-link="<?= $link; ?>"><?= $title; ?></a>
                                </li>
                            <?php
                                }
                            ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php
    }
?>

<!--HEADER-->
<section class="white-bg">
    <div class="container">
        <div class="panel local-content team-filter-container <?php if ($have_filters) { echo 'padding-0-top';} else { echo 'padding-top';}?>">
            <?php if ($section): ?>
                <div class="panel-body blue-border blue-border-top">
                    <div class="row">
                        <div class="col-md-12">
                            <h2 class="indigo-900 margin-0-top">
                                <?php echo $section['title']; ?>
                                <?php if ($section['subtitle']): ?>
                                    <span class="subtitle f-24">
                                        <?php echo $section['subtitle']; ?>
                                    </span>
                                <?php endif; ?>
                            </h2>
                            <?php echo $section['body']; ?>
                        </div>
                    </div>
                </div>
            <?php endif; ?>

            <?php the_cta(CTA_POSITION_TOP); ?>

            <?php if( have_rows('teams') ): ?>
            <?php while ( have_rows('teams') ) : the_row(); ?>

            <div class="text-center">
                <h2 class="indigo-900">
                    <?php echo get_sub_field('team_name'); ?>
                    <span class="f-24 subtitle">
                        <?php echo get_sub_field('team_description'); ?>
                    </span>
                </h2>
                <?php $row_total = count(get_sub_field('team_members')); ?>
                <?php if( have_rows('team_members') ): ?>
                    <div class="row sm-up-row-equal-height">
                        <?php while (have_rows('team_members')): the_row();
                            set_query_var('member', get_sub_field('member'));

                            $userLink = get_user_meta(get_sub_field('member')['ID'], 'user_location', true); ?>

                            <div class="col-md-4 col-sm-12 team-filter" data-link="<?= $userLink; ?>">
                                <div class="col-md-12 col-sm-12 panel service service-narrow white">
                                    <div class="panel-body">
                                        <?php get_template_part('includes/team-panel', 'team-panel'); ?>
                                    </div>
                                </div>
                            </div>
                        <?php endwhile; ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>

        <?php break; ?>

        <?php endwhile; ?>
        <?php endif; ?>
    </div>
</section>

<section class="white-bg">

    <?php if( have_rows('teams') ): ?>
        <?php while ( have_rows('teams') ) : the_row(); ?>

            <div class="container text-center team-filter-container">

                <h2 class="indigo-900">
                    <?php echo get_sub_field('team_name'); ?>
                    <span class="subtitle f-24"><?php echo get_sub_field('team_description'); ?></span>
                </h2>

                <?php $row_total = count(get_sub_field('team_members')); ?>
                <?php if( have_rows('team_members') ): ?>
                    <div class="row sm-up-row-equal-height">
                        <?php while (have_rows('team_members')): the_row();
                            set_query_var('member', get_sub_field('member'));
                            $userLink = get_user_meta(get_sub_field('member')['ID'], 'user_location', true); ?>
                            <div class="col-md-3 col-sm-12 team-filter" data-link="<?= $userLink; ?>">
                                <div class="col-md-12 col-sm-12 panel service blue service-narrow">
                                    <div class="panel-body">
                                        <?php get_template_part('includes/team-panel', 'team-panel'); ?>
                                    </div>
                                </div>
                            </div>
                        <?php endwhile; ?>
                    </div>
                <?php endif; ?>


            </div>

        <?php endwhile; ?>
    <?php endif; ?>

</section>

<!-- Modal -->
<div class="modal fade" id="who-we-are-modal" tabindex="-1" role="dialog" aria-labelledby="whoWeAreModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="row">
                <div class="col-sm-5 col-xs-12">
                    <!-- Dynamic Image -->
                    <img src="" id="profile-img" class="img-responsive margin-top"/>
                </div>
                <div class="visible-xs col-xs-12">
                    <p class="margin-min-top">
                        <a data-toggle="collapse" href="#bio" aria-expanded="false" aria-controls="collapseExample">Read Bio...</a>
                    </p>
                    <div class="biography collapse" id="bio">
                        <!-- Dynamic Content -->
                    </div>
                </div>
                <div aria-labelledby="biography" class="biography visible-sm visible-md visible-lg modal-body col-sm-7 col-xs-12 f-18 margin-top in">
                    <!-- Dynamic Content -->
                </div>
            </div>
            <div class="modal-footer row">
                <div class="col-sm-6 col-xs-12">
                    <a href="tel://<?= str_replace(' ', '', get_field('contact_telephone', 'option')); ?>" class="f-18 margin-min-bottom btn btn-block btn-primary">
                        <i class="fa fa-phone fa-fw" aria-hidden="true"></i>Call us on <?= get_field('contact_telephone', 'option'); ?>
                    </a>
                </div>
                <div class="col-sm-6 col-xs-12">
                    <a id="user-email" class="f-18 margin-base-bottom btn btn-block btn-primary"><i class="fa fa-send fa-fw" aria-hidden="true"></i>Send me an email</a>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <i class="fa fa-times-circle"></i>
                </button>
            </div>
        </div>
    </div>
</div>

<!-- CALL TO ACTION -->
<?php the_cta(CTA_POSITION_BOTTOM, [
    'before_cta' => '<section class="white-bg padding-top padding-bottom"><div class="container">',
    'after_cta' => '</div></section>'
]); ?>
<!-- END -->

<?php get_footer(); ?>
