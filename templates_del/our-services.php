<?php
/**
 * Template Name: Our Services
 */
?>

<?php
    get_header();

    $navItems = getSiblingNavigationItems($post);

    $currId = get_queried_object_id();
    $hero = get_the_post_thumbnail_url($currId);

    $cookieOrigin = $_COOKIE['origin'];
?>

<div class="block-image screen-height block-image-responsive-shift" style="background-image: url('<?= $hero; ?>');"></div>

<!--CAROUSEL NAV BUTTONS-->
<div class="fixed full-width page-carousel-nav">
    <a href="<?php echo get_permalink($navItems['previous']); ?>" title="<?php echo get_the_title($navItems['previous']); ?>">
        <div class="hidden-xs page-carousel-nav-left blue-700-bg padding-min-top padding-min-right padding-min-bottom padding-min-left">
            <i class="fa fa-chevron-left white"></i>
        </div>
    </a>
    <a href="<?php echo get_permalink($navItems['next']); ?>" title="<?php echo get_the_title($navItems['next']); ?>">
        <div class="hidden-xs page-carousel-nav-right blue-700-bg padding-min-top padding-min-right padding-min-bottom padding-min-left">
            <i class="fa fa-chevron-right white"></i>
        </div>
    </a>
</div>

<!--CONTENT-->
<section class="service-page">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-9 col-sm-offset-1 col-md-offset-1 col-md-7 white-bg-opq-80 padding-left-responsive padding-right-responsive">
                <h1><?php the_title(); ?></h1>
                <?php the_content(); ?>
            </div>
            <div class="col-xs-12 col-sm-2 col-md-1 col-md-offset-3 text-center margin-top margin-bottom">

                <a href="tel://<?php echo str_replace(' ', '', get_field('contact_telephone', 'option')); ?>">
                    <span class="fa-stack f-40">
                        <i class="fa fa-circle fa-stack-2x white-opq-60"></i>
                        <i class="fa fa-phone fa-stack-1x indigo-900"></i>
                    </span>
                </a>
                <a href="<?= $cookieOrigin; ?>contact-us">
                    <span class="fa-stack f-40">
                        <i class="fa fa-circle fa-stack-2x white-opq-60"></i>
                        <i class="fa fa-send fa-stack-1x indigo-900"></i>
                    </span>
                </a>
            </div>
        </div>
    </div>
</section>
<?php if (get_field('horizontal_box')): ?>
    <section class="service-page white-bg-opq-80">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-9 col-sm-offset-1 col-md-offset-1 col-md-7 white-bg-opq-80 padding-top padding-bottom">
                    <?= get_field('horizontal_content'); ?>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>

<?php get_footer(); ?>
