<?php
/**
 * Template Name: Blog
 */
?>

<?php get_header() ?>

<?php while ( have_posts() ) : the_post() ?>

    <section class="white-bg">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h2 class="blue-grey-900">Education zone</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-md-8">
                    <?php get_template_part('includes/carousel-full') ?>

                    <div class="row">
                        <div class="col-xs-12">
                            <h3 class="blog-header">Latest topics</h3>
                        </div>
                    </div>
                    <div class="row sm-up-row-equal-height">
                        <?php
                            set_query_var('blogEntryColor', "cyan");
                            set_query_var('colMd', "6");
                            $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
                            $my_query_args = array(
                                'posts_per_page' => 4,
                                'post_type' => 'post',
                                'paged' => $paged
                            );
                            $my_query = new WP_Query( $my_query_args );

                            if( $my_query->have_posts() ) : while( $my_query->have_posts() ) : $my_query->the_post();

                                $cats = get_the_category();
                                foreach( $cats as $cat ){
                                    $cat_name = $cat -> cat_name;
                                }
                                get_template_part('includes/category-post-list', 'category-post-list');
                            endwhile;
                            endif;


                        if (function_exists(custom_pagination)) {
                            custom_pagination($my_query->max_num_pages,"",$paged);
                        }
                        wp_reset_postdata();
                        ?>
                    </div>
                </div>
                <div class="col-xs-12 col-md-4">
                    <?php get_template_part('includes/blog-side') ?>
                </div>
            </div>
        </div>
    </section>

<?php endwhile ?>

<?php get_footer() ?>