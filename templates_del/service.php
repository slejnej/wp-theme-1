<?php
/*
 * Template Name: Service Page
 */
?>

<?php get_header(); ?>

<section class="white-bg padding-top padding-bottom">
    <div class="container">
        <h2><?php the_title(); ?></h2>
        <?php the_content(); ?>
    </div>
</section>

<?php the_cta(CTA_POSITION_BOTTOM); ?>

<?php get_footer(); ?>
