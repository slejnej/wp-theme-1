<?php
/**
 * Template Name: Homepage
 */
?>
<?php get_template_part("includes/cookie-set"); ?>

<?php get_header();?>

<div class="block-image screen-height" style="background-image: url('/wp-content/themes/partner-wp-theme/assets/avada-theme/images/FC170210-Partnerships-prebg.jpg')"></div>

<!--HEADER-->
<section class="white-bg">
    <div class="container local-hero" style="background-image: url('<?php echo get_field('hero_local_image'); ?>');">
        <div class="row content">
            <div class="col-xs-12 white">
                <div>
                    <h2>
                        <?php echo get_field('hero_local_title'); ?>
                        <span class="subtitle"><?php echo get_field('hero_local_subtitle'); ?></span>
                    </h2>
                </div>
            </div>
            <div class="col-md-10 white">
                <?php echo get_field('hero_local_content'); ?>
            </div>
            <div class="col-md-2 white">
                <?php the_cta(CTA_POSITION_TOP); ?>
            </div>
        </div>
    </div>
</section>

<!--ADVISER SEARCH-->
<section class="white-bg" id="advice">
    <div class="container">
        <div class="row margin-top">
            <div class="col-xs-12">
                <h3 class="blue-500"><?php echo get_field('callout_title'); ?></h3>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-md-6">
                <p class="indigo-900">
                    <?php echo get_field('callout_subtitle'); ?>
                </p>
                <div class="row">
                    <div class="col-xs-12 col-md-8">
                        <form action="/find-an-adviser" method="post">
                            <div class="row margin-min-top">
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <input name="postcode" type="text" class="form-control full-width" placeholder="Your post code">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <button class="btn btn-block indigo-a700-bg">
                                        <i class="icon-graduation-cap"></i>
                                        <?php echo get_field('callout_button'); ?>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-md-6">
                <p class="indigo-900">
                    <?php echo get_field('callout_list_title'); ?>
                </p>
                <?php if( have_rows('callout_list_items') ): ?>
                    <ul>
                        <?php while (have_rows('callout_list_items')): the_row(); ?>
                            <li><?php echo get_sub_field('callout_entry'); ?></li>
                        <?php endwhile; ?>
                    </ul>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>

<!--SERVICES-->
<?php if( have_rows('services') ): ?>
<section class="white-bg padding-top padding-bottom">
    <div class="container text-center">
        <h2 class="indigo-900">
            <?php echo get_field('service_title'); ?>
            <span class="subtitle"><?php echo get_field('service_subtitle'); ?></span>
        </h2>
        <div class="row sm-up-row-equal-height">
            <?php while (have_rows('services')): the_row(); ?>
            <div class="col-sm-6 col-md-4">
                <div class="panel service blue">
                    <div class="panel-body">
                        <!-- get featured image then override with service_image if exists-->
                        <?php
                        $currId = get_queried_object_id();
                        $featPageImage = get_the_post_thumbnail_url(get_sub_field('service'), 'thumbnail');
                        $image = $featPageImage ?: get_sub_field('service_image');
                        ?>
                        <img src="<?= $image ?>" class="img-responsive center-block">
                        <h3><?php echo get_sub_field('service')->post_title; ?></h3>
                        <p><?php echo get_sub_field('service_description'); ?></p>
                        <div class="footer-element">
                            <a class="btn btn-more" href="<?php echo get_permalink(get_sub_field('service')); ?>">See more <i class="fa fa-hand-o-right f-20"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <?php endwhile; ?>
        </div>
    </div>
</section>
<?php endif; ?>

<!--FEATURED IN BANNER-->
<section class="light-blue-900-bg-opq-60 white padding-top padding-bottom">
    <div class="container text-center">
        <div class="row">
            <div class="col-xs-12">
                <h2><?php echo get_field('featured_title'); ?></h2>
                <div class="panel-body featured-in-body">
                    <?php foreach (get_field('featured') as $featured_in): ?>
                    <img class="img-responsive center-block" src="<?php echo $featured_in['sizes']['medium']; ?>" />
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</section>

<!--TESTIMONIALS-->
<?php if( have_rows('testimonials') ): ?>
<section class="white-bg padding-top padding-bottom">
    <div class="container text-center margin-bottom padding-bottom">
        <h2 class="indigo-900">
            <?= get_field('testimonial_title'); ?>
            <span class="subtitle"><?php echo get_field('testimonial_subtitle'); ?></span>
        </h2>
        <div class="row sm-up-row-equal-height">
            <?php while (have_rows('testimonials')): the_row(); ?>
            <div class="col-md-4 col-xs-12">
                <div class="drop-shadow testimonial-item margin-bottom">
                    <div class="stars">
                        <?php for ($i = 0; $i < 5; $i++): ?>
                            <?php if (get_sub_field('testimonial_rating') > $i): ?>
                                <span class="fa fa-star fa-lg"></span>
                            <?php else: ?>
                                <span class="fa fa-star-o fa-lg"></span>
                            <?php endif; ?>
                        <?php endfor; ?>
                    </div>
                    <p class="comment">
                        <q><?= get_sub_field('testimonial_quote'); ?></q>
                    </p>
                    <p class="signature"><b><?= get_sub_field('testimonial_author'); ?>,</b> <?= get_sub_field('testimonial_author_city'); ?></p>
                    <div class="triangle-topleft"></div>
                </div>
            </div>
            <?php endwhile; ?>
        </div>
    </div>
</section>
<?php endif; ?>

<!--BANNER-->
<?php the_cta(CTA_POSITION_MIDDLE, [
    'before_cta' => '<section class="padding-min-top out-of-bounds" style="background-image: url(\'wp-content/themes/partner-wp-theme/assets/avada-theme/images/bnr-bg.jpg\');">
    <div class="container">
        <div class="row">
            <div class="col-sm-8">
                <h2 class="no-gutter">
                    Get an instant personal financial plan
                    <span class="subtitle">with helpful tips on how to plan for your future</span>
                </h2>
            </div>
            <div class="col-sm-4">
                <img src="wp-content/themes/partner-wp-theme/assets/avada-theme/images/bnr-FFP.png" alt="Free financial plan" class="out-of-bounds">
            </div>
        </div>',
    'after_cta' => '</div></section>',
    'classes' => 'bottom-outside'
]); ?>

<!--POSTS / BLOG from custom fields -->
<?php if( have_rows('blog_entries') ): ?>
<section class="white-bg padding-top padding-bottom">
    <div class="container padding-top">
        <h2 class="indigo-900 text-center">
            <?= get_field('blog_section_title'); ?>
                        <span class="subtitle"><?= get_field('blog_section_subtitle'); ?></span>
        </h2>
        <div class="row sm-up-row-equal-height">
            <?php
                $posts = get_field('blog_entries');
                set_query_var('blogEntryColor', "blue");
                set_query_var('colMd', "4");

                if( $posts ):
                    // switch to multisite
                    switch_to_blog( $posts['site_id'] );
                    foreach ($posts['selected_posts'] as $post): setup_postdata($post);
                        get_template_part('includes/category-post-list', 'category-post-list');
                    endforeach;

                    wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly
                    restore_current_blog(); // IMPORTANT switch back to current site
                endif;
            ?>
        </div>
    </div>
</section>

<?php endif; ?>

<!--Customizable content section-->
<?php if ( have_rows('content_sections') ): ?>
<section class="white-bg">
    <div class="container">
        <div class="panel local-content-inverse">
            <div class="panel-body">
                <?php get_section(); ?>
            </div>
        </div>
    </div>
</section>
<?php endif; ?>

<?php the_cta(CTA_POSITION_BOTTOM, [
    'before_cta' => '<section class="white-bg padding-top padding-bottom"><div class="container">',
    'after_cta' => '</div></section>'
]); ?>

<?php get_footer(); ?>
