<?php
/**
 * Template Name: How We Invest
 */
?>

<?php get_header(); ?>
<!--TYPES TABS-->
<section class="white-bg padding-top">
    <div class="container">
        <div class="text-center">
            <h2 class="indigo-900 margin-min-bottom">
                We offer 10 diversified investment portfolios that suit different attitudes to risk.
            </h2>
            <p>
                There are five in the 'Core range' and five portfolios in the 'Dynamic range'. Our financial advisers
                help select the most suitable one for you. You can see the funds our portfolios invest in here. The
                chart below shows how each of our portfolios compares to our competitors using the ARC index.
            </p>
        </div>
    </div>
</section>

<portfolio-history></portfolio-history>

<!--DETAILS-->
<section>
    <div class="container">
        <div class="text-center">
            <h2 class="indigo-900 margin-min-bottom">
                Affordable, diversified investments, managed with discipline
            </h2>
            <p>
                Investment markets can be noisy, confusing places, often drive by short-term fear of greed rather than
                long-term planning.<br/>
                At Flying Colours we have three core principles that underpin our approach to investing. We'll never
                deviate from them.
            </p>
        </div>
        <div class="panel">
            <div class="panel-body">
                <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-4 text-center">
                        <span class="fa-stack fa-lg blue-500">
                            <i class="fa fa-circle fa-stack-2x"></i>
                            <i class="fa icon-thumbs-up fa-stack-1x fa-inverse"></i>
                        </span>
                        <h5 class="blue-800">Discipline</h5>
                        <p>
                            We take a patient approach to investing. We ignore short-term distractions in order to find investments with long-term value.
                        </p>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-4 text-center">
                        <span class="fa-stack fa-lg blue-500">
                            <i class="fa fa-circle fa-stack-2x"></i>
                            <i class="fa icon-picture fa-stack-1x fa-inverse"></i>
                        </span>
                        <h5 class="blue-800">Diversification</h5>
                        <p>
                            We invest money across a range of funds and asset classes. This is a great, proven way to reduce investment risk.
                        </p>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-4 text-center">
                            <span class="fa-stack fa-lg blue-500">
                                <i class="fa fa-circle fa-stack-2x"></i>
                                <i class="fa icon-trophy fa-stack-1x fa-inverse"></i>
                            </span>
                        <h5 class="blue-800">Low-cost funds</h5>
                        <p>
                            The less you pay in charges, the more of your money gets invested. More money invested means higher potential returns.
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 text-center margin-top">
                        <a class="btn orange-a700-bg" href="/contact-us"><i class="fa fa-thumbs-o-up"></i> Request a call back</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!--BY MYSELF-->
<section class="blue-50-bg blue-border padding-min-top margin-bottom">
    <div class="container">
        <div class="text-center">
            <h2 class="indigo-900">
                Couldn't I just do this all myself?
            </h2>
            <p class="padding-min-bottom">
                We think that investing for you future is too important to be a part-time hobby. Our professionals
                service offers a number of advantages over a DIY approach. Even the most experienced DIY investors
                rarely find the time to manage their investments as closely as they would like. We'll give you back
                the time to focus on the things that really matter to you. And our low fees and investment discipline
                will help to put more money back in your pocket.
            </p>
        </div>
        <div class="row">
            <div class="col-xs-12 col-md-6 text-center">
                <div class="white-bg blue-border margin-min padding-min">
                    <h3 class="indigo-900 f-30">Monitoring</h3>
                    <p>
                        We regularly review your portfolio to ensure it stays
                        within your agreed attitude to risk category.
                    </p>
                </div>
            </div>
            <div class="col-xs-12 col-md-6 text-center">
                <div class="white-bg blue-border margin-min padding-min">
                    <h3 class="indigo-900 f-30">Expert decision-making</h3>
                    <p>
                        Our investment team have many years' experience in
                        making decisions through the economic cycle.
                    </p>
                </div>
            </div>
        </div>
        <div class="row margin-min-bottom">
            <div class="col-xs-12 col-md-6 text-center">
                <div class="white-bg blue-border margin-min padding-min">
                    <h3 class="indigo-900 f-30">Access</h3>
                    <p>
                        We have access to a broader range of investments at
                        lower prices than are available to a private investor.
                    </p>
                </div>
            </div>
            <div class="col-xs-12 col-md-6 text-center">
                <div class="white-bg blue-border margin-min padding-min">
                    <h3 class="indigo-900 f-30">Portfolio design</h3>
                    <p>
                        All our portfolios are designed to achieve the best
                        possible returns for the level of risk acceptable to you.
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>

<!--OUR INVESTORS-->
<section class="white-bg margin-bottom">
    <div class="container">
        <div class="text-center">
            <h2 class="indigo-900">
                Our investment team
            </h2>
            <p class="padding-bottom">
                This group meets regularly to ensure that our portfolios continue to match the risk profiles we have defined and to which your investment will be aligned.
            </p>

            <a class="btn light-blue-a700-bg" href="/who-we-are"><i class="fa fa-comments-o"></i> Meet our team</a>
        </div>
    </div>
</section>
<script type="text/javascript" src="<?php echo PORTFOLIO_PERFORMANCE_URL; ?>/inline.bundle.js"></script>
<script type="text/javascript" src="<?php echo PORTFOLIO_PERFORMANCE_URL; ?>/polyfills.bundle.js"></script>
<script type="text/javascript" src="<?php echo PORTFOLIO_PERFORMANCE_URL; ?>/styles.bundle.js"></script>
<script type="text/javascript" src="<?php echo PORTFOLIO_PERFORMANCE_URL; ?>/vendor.bundle.js"></script>
<script type="text/javascript" src="<?php echo PORTFOLIO_PERFORMANCE_URL; ?>/main.bundle.js"></script>
<?php get_footer(); ?>
