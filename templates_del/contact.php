<?php
/**
 * Template Name: Contact Us
 */
?>

<?php get_header(); ?>

<!--HEADER-->
<section class="white-bg padding-top">
    <div class="container">
        <div class="panel local-content">
            <div class="panel-body blue-border blue-border-top">
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-3 text-center">
                            <span class="fa-stack fa-lg blue-500">
                                <i class="fa fa-circle fa-stack-2x"></i>
                                <i class="fa fa-map-signs fa-stack-1x fa-inverse"></i>
                            </span>
                        <h5 class="blue-800">Address</h5>
                        <?php $partner = get_field('contact_options', 'option')[0]; ?>
                        <p>
                            <?php echo str_replace(',', '<br />', $partner['partner_street']); ?>
                            <?php echo $partner['partner_locality']; ?><br/>
                            <?php echo $partner['partner_postalcode']; ?><br/>
                        </p>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-3 text-center">
                            <span class="fa-stack fa-lg blue-500">
                                <i class="fa fa-circle fa-stack-2x"></i>
                                <i class="fa fa-clock-o fa-stack-1x fa-inverse"></i>
                            </span>
                        <h5 class="blue-800">Business hours</h5>
                        <p>
                            Monday- Friday<br/>
                            9:00am - 6:30pm
                        </p>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-3 text-center">
                            <span class="fa-stack fa-lg blue-500">
                                <i class="fa fa-circle fa-stack-2x"></i>
                                <i class="fa fa-phone fa-stack-1x fa-inverse"></i>
                            </span>
                        <h5 class="blue-800">Call us</h5>
                        <p>
                            Speak to our advice team:<br/>
                            <a href="tel://<?php echo str_replace(' ', '', get_field('contact_telephone', 'option')); ?>"><?php echo get_field('contact_telephone', 'option'); ?></a>
                        </p>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-3 text-center">
                            <span class="fa-stack fa-lg blue-500">
                                <i class="fa fa-circle fa-stack-2x"></i>
                                <i class="fa fa-send-o fa-stack-1x fa-inverse"></i>
                            </span>
                        <h5 class="blue-800">Email</h5>
                        <p>
                            <a href="mailto:<?php echo get_field('contact_email', 'option'); ?>"><?php echo get_field('contact_email', 'option'); ?></a>
                        </p>
                    </div>
                </div>
            </div>
            <?php the_cta(CTA_POSITION_TOP); ?>
        </div>
    </div>
</section>

<!--CONTACT US-->
<section class="white-bg padding-top padding-bottom">
    <div class="container">
        <?php the_content(); ?>
    </div>
</section>

<!--CALLBACK-->
<section id="request-callback" class="padding-min-top white" style="background-image: url('<?= get_bloginfo("template_url"); ?>/images/ws-contact-BG2.jpg'); background-size: cover;">
    <div class="container padding-bottom">
        <div class="row">
            <div class="col-xs-12 col-md-6">
                <div id="fc-callback">
                    <div class="col-xs-12 ng-cloak" ng-hide="saved">
                        <div class="row">
                            <h5 class="blue-a100">
                                Speak to our advice team on <a href="tel://<?php echo str_replace(' ', '', get_field('contact_telephone', 'option')); ?>" class="f-36 white"><?php echo get_field('contact_telephone', 'option'); ?></a> or<br/>
                            </h5>
                            <h3>Request a call back</h3>
                        </div>
                    </div>
                    <div class="col-xs-12 section-thankyou ng-cloak" ng-show="saved">
                        <div class="row">
                            <p>Thanks for sending your details through.</p>
                            <p>We’ll get in touch as soon as possible. We will call you on your phone number.</p>
                            <p>If you have any more queries, please take a look at our <a href="/education">Education Zone</a> and our <a href="/faqs">FAQs</a>.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-md-6">
                <div class="map"
                     data-marker-image="/wp-content/themes/partner-wp-theme/assets/avada-theme/images/map-pin.png"
                     data-map-lat="<?php echo get_field('branch_address', 'option')['lat']; ?>"
                     data-map-long="<?php echo get_field('branch_address', 'option')['lng']; ?>"></div>
            </div>
        </div>
    </div>
    <div class="container-fluid panel local-content-inverse footer">
        <div class="panel-body">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <p class="f-16">
                            We will only use the information you provide with our <a href="/legal/privacy-policy/?partner=east">Privacy Policy</a>. By submitting your information you have accepted this policy.
                        </p>
                    </div>
                </div>
            </div>
        </div>
</section>

<!--CONTACT US-->
<?php if ( have_rows('content_sections') ): ?>
<section class="white-bg padding-bottom">
    <div class="container">
        <div class="panel">
            <div class="panel-body blue-border">
                <div class="row">
                    <div class="col-md-12">
                        <?php get_section(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php endif; ?>

<?php get_footer(); ?>
