<?php
/**
 * Template Name: FAQs
 */
?>

<?php get_header() ?>

<?php while ( have_posts() ) : the_post() ?>

	<section class="white-bg">
		<div class="container">
            <div class="row">
                <div class="col-sm-4 col-md-4 hidden-xs">
                    <div class="left-nav">
                        <h4 class="blue-grey-500">Quick links</h4>
                        <?php wp_nav_menu( array(
                            'menu' => 'FAQs left nav',
                            'container' => '',
                            'menu_class' => 'nav'
                        )); ?>
                    </div>
                </div>
                <div class="col-xs-12 visible-xs">
                    <span class="scroll-to">
                        <a role="button" class="btn btn-primary" href="#mob-panel" id="to-top"><i class="fa fa-chevron-up"></i></a>
                    </span>
                    <div class="panel" id="mob-panel">
                        <div class="panel-heading">
                            <a role="button" data-toggle="collapse" class="collapsed btn-block" href="#mob-nav">
                                FAQs
                                <i class="fa pull-right"></i>
                            </a>
                        </div>
                        <div id="mob-nav" class="panel-collapse collapse" role="tabpanel">
                            <?php wp_nav_menu( array(
                                'menu' => 'FAQs left nav',
                                'container' => '',
                                'menu_class' => 'nav side-nav'
                            )); ?>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-8 col-md-8 margin-bottom">
                    <div class="content accordion-list">
                        <?php
                            $page = get_page_by_title( 'faqs' );
                            $content = apply_filters('the_content', $page->post_content);
                            echo $content;
                        ?>
                    </div>
                </div>
            </div>
		</div>
	</section>


<?php endwhile ?>

<?php get_footer() ?>