<?php
/**
 * Template Name: Page with left NAV
 */
?>

<?php get_header() ?>

<?php
    global $post;
    $page = get_post($post->post_parent);

    while (have_posts()) : the_post();
?>
    <section class="white-bg">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="row">
                        <?php if (is_page() && $post->post_parent) { $page = get_post($post->post_parent); ?>
                            <ol class="breadcrumb">
                                <li>
                                    <a href="<?php echo get_permalink($page->ID) ?>"><?php echo get_the_title($page->ID); ?></a>
                                </li>
                                <li>
                                    <?= the_title(); ?>
                                </li>
                            </ol>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4 col-md-4 hidden-xs">
                    <div class="left-nav" style="margin-top: 0;">
                        <h4 class="blue-grey-500">Legal</h4>
                        <?= list_child_pages(); ?>
                    </div>
                </div>
                <div class="col-xs-12 visible-xs">
                    <div class="scroll-to">
                        <a role="button" class="btn btn-primary" href="#mob-panel" id="to-top"><i class="fa fa-chevron-up"></i></a>
                    </div>
                    <div class="panel" id="mob-panel">
                        <div class="panel-heading">
                            <a role="button" data-toggle="collapse" class="collapsed btn-block" href="#mob-nav">
                                <?= the_title(); ?>
                                <i class="fa pull-right"></i>
                            </a>
                        </div>
                        <div id="mob-nav" class="panel-collapse collapse" role="tabpanel">
                            <?= list_child_pages(); ?>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-8 col-md-8 margin-bottom">
                    <div class="content">
                        <h2><strong><?= the_title(); ?></strong></h2>
                        <?= the_content(); ?>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php endwhile; ?>

<?php get_footer() ?>