<?php
/**
 * Template Name: Partnership
 */
?>

<?php get_header() ?>

<?php while ( have_posts() ) : the_post() ?>
    <div class="green-800">
        <section class="white-bg">
            <div class="container margin-top">
                <div class="row">
                    <div class="col-xs-12 col-md-11">
                        <h2 class="f-36 text-center">
                            Flying Colours is a directly authorised Independent Financial Advice firm. Our mission is to evolve the way the investment and financial advice market works, to the benefit of all stakeholders - clients and advisers, alike.
                        </h2>
                    </div>
                </div>
            </div>
        </section>
        <section class="light-green-100-bg margin-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-4 hidden-xs">
                        <div class="overlay-wrapper">
                            <img alt="Rod Stewart's Picture" class="img-responsive" src="/wp-content/themes/partner-wp-theme/images/FC-Rod-col-x262.jpg" />
                            <div class="overlay inset-shadow"></div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-8">
                        <div class="f-22 margin-top margin-min-bottom">
                            <q class="lg-quote"> All Flying Colours financial advisers share the same vision: to maximise returns for clients through best-in-class solutions and individually tailored investment options that negate the hidden costs plaguing the industry. </q>
                            <cite class="visible-xs visible-sm"> - Rod Stewart, COX Partnerships</cite>
                        </div>
                        <div class="row">
                            <p class="col-md-6 f-18 margin-min-top hidden-xs hidden-sm">
                                - <strong>Rod Stewart, COX Partnerships</strong>
                            </p>
                            <div class="col-md-6 col-xs-12 margin-min-bottom text-center">
                                <a href="/contact-us/" class="btn green-a700-bg"><i class="fa fa-thumbs-up"></i> Let's work together</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="bg-white margin-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-offset-1 col-sm-10">
                        <p>
                            We invite experienced independent financial advisers looking to grow their business and secure a succession plan, to contact us about how we can help them increase the value of their business through proven best practices and technological efficiencies.
                        </p>
                        <p>
                            We believe many advisers are focused on inefficient tasks within their business. We work with you to provide the structure, support, and funding required; so you can focus on what you do best - generating value for your clients and yourself.
                        </p>
                    </div>
                </div>
                <div class="row">
                    <h2 class="col-xs-12 text-center">Why you, as a high calibre IFA, need to talk to us:</h2>
                </div>
                <ol class="partner row sm-up-row-equal-height list-unstyled margin-bottom">
                    <li class="col-xs-12 col-sm-4 text-center">
                        <div class="green-600-bg filter-cloud margin padding-min">
                            <h3>Focus on efficiency</h3>
                            <p>
                                We provide the support mechanisms to allow you to focus on serving your clients and building your business
                            </p>
                        </div>
                    </li>
                    <li class="col-xs-12 col-sm-4 text-center">
                        <div class="green-600-bg filter-cloud margin padding-min">
                            <h3>Remove the hassle</h3>
                            <p>
                                Leverage our systems and processes to negate administrative waste and overheads
                            </p>
                        </div>
                    </li>
                    <li class="col-xs-12 col-sm-4 text-center">
                        <div class="green-600-bg filter-cloud margin padding-min">
                            <h3>Grow with Flying Colours</h3>
                            <p>
                                Get introduced to high value clients. Use our systems to manage and build more client relationships through initiatives like our client lead generation program.
                            </p>
                        </div>
                    </li>
                </ol>
                <div class="row">
                    <div class="col-xs-12 col-sm-10 col-sm-offset-1">
                        <p>
                            As we build up to the official launch in early September - featuring a comprehensive new Partnership area with IFA tools and resources - we encourage you to <a class="green-500"  href="/contact-us/">register your interest here</a>.
                        </p>
                        <p>
                            We'll keep you updated as we progress, and would like to talk to you about how we can deliver our first class partnership and service offering.
                        </p>
                    </div>
                </div>
            </div>
        </section>
        <section class="bg-white">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-10 col-md-offset-1">
                        <div class="row phone-bg">
                            <div class="col-xs-12 col-sm-offset-4 col-sm-7 margin-bottom">
                                <h3>Interested in finding out more?</h3>
                                <p>
                                    If you would like to find out more,<br />
                                    please call Rod Stewart<br />
                                    on <a class="green-800" href="tel://07456982419">07456 982 419</a>.
                                </p>
                            </div>
                            <div class="text-center margin-bottom col-xs-12 col-sm-6 col-sm-offset-6">
                                <a class="btn green-a700-bg fa-16" href="/contact-us/"><i class="fa fa-thumbs-up"></i> Let's work together</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

<?php endwhile ?>

<?php get_footer() ?>
