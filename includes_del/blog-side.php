<!-- INFO-->
<div class="blog-widget">
    <div class="info-widget">
        <i class="fa fa-info-circle fa-lg green-500"></i>
        <p>
            We hope you find the education zone interesting and useful, but please remember it shouldn’t
            be viewed as financial advice.
        </p>
    </div>
</div>
<!-- POPULAR POSTS -->
<div class="blog-widget margin-top">
    <h5>Popular posts</h5>
    <div class="post-category-widget">
        <div class="row sm-up-row-equal-height">
            <?php
            $popularPostArgs = [
                'posts_per_page' => 2,
                'post_type' => 'post',
                'meta_key' => 'wpb_post_views_count',
                'orderby' => 'meta_value_num',
                'order' => 'DESC'
            ];
            $popularPost = new WP_Query( $popularPostArgs );
            if( $popularPost->have_posts() ) : while( $popularPost->have_posts() ) : $popularPost->the_post();

                $cats = get_the_category();
                foreach( $cats as $cat ){
                    $cat_name = $cat -> cat_name;
                }
                ?>
                <div class="col-xs-6 ">
                    <a href="<?= get_post_permalink($post->ID); ?>" title="<?= the_title(); ?>">
                        <img src="<?= wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>" class="img-responsive" alt="<?= the_title(); ?>">
                    </a>
                    <p class="post-date"><?= mysql2date('d.m.Y', $post->post_date); ?></p>
                    <a class="post-title cyan-500" href="<?= get_post_permalink($post->ID); ?>" rel="bookmark"><?= the_title(); ?></a>
                </div>
                <?php
            endwhile;
            endif;

            wp_reset_postdata();
            ?>
        </div>
    </div>
</div>
<!-- CATEGORIES LIST -->
<div class="blog-widget margin-top">
    <h5>Browse by category</h5>
    <?php
        $categories = get_categories(
            [ 'parent' => 1 ]
        );

        foreach ($categories as $cat) {
    ?>
        <a href="/blog/category/<?= $cat->slug; ?>" class="btn btn-block cyan-500-bg"><?= $cat->name; ?></a>
    <?php
        }
    ?>
</div>