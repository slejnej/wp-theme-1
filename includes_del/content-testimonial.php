<div class="row">
    <?php if (isset($author['image_url'])): ?>
    <div class="col-md-3">
        <img src="<?php echo $author['image_url']; ?>" class="img-responsive center-block">
    </div>
    <div class="col-md-9">
    <?php else: ?>
    <div class="col-md-12">
    <?php endif; ?>
        <?php if( ! empty($title) ): ?>
        <h2 class="indigo-900">
            <?php echo $title; ?>
            <?php if ( ! empty($subtitle)): ?>
                <span class="subtitle"><?php echo $subtitle; ?></span>
            <?php endif; ?>
        </h2>
        <?php endif; ?>
        <?php echo $body; ?>
        <div class="signature with-padding" style="background-image: url(<?php echo $author['signature']; ?>)">
            - <?php echo sprintf('%s, %s', $author['display_name'], $author['role']); ?>, <span><?php bloginfo('title'); ?></span>
            <?php if ( $author['social']['linkedin']): ?>
                <div class="pull-right">
                    <a href="<?php echo $author['social']['linkedin']; ?>" target="_blank" class="btn btn-linkedin margin-base-left">
                        <i class="fa fa-linkedin white"></i>
                    </a>
                </div>
            <?php endif; ?>
        </div>
    </div>