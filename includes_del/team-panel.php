<?php
    $picture_id = get_user_meta($member['ID'], 'profile_public_picture', true);
    $profile_picture_url = getAttatchmentMetadata($picture_id);
    $profile_picture_src = $profile_picture_url ? sprintf('%s/%s', '/wp-content/uploads', $profile_picture_url)
        : 'http://placehold.it/278x278';
    $user_bio = get_user_meta($member['ID'], 'bio', true);
?>
<div class="panel-body">
    <img src="<?php echo $profile_picture_src; ?>" class="img-responsive center-block" data-img="<?= $member['user_nicename']; ?>">
    <h3 class="f-24"><?php echo $member['display_name'];  ?></h3>
    <p>
        <?php echo get_user_meta($member['ID'], 'job_title', true); ?><br>
        <?php echo get_user_meta($member['ID'], 'qualification', true); ?>
    </p>

    <div class="footer-element">
        <?php if (!empty($user_bio)) {?>
            <button class="btn btn-square" data-modal="true" data-user-email="<?= $member['user_email']; ?>" data-modal-content="<?= $member['user_nicename']; ?>"><i class="fa fa-eye f-24"></i></button>
        <?php } ?>
    </div>

    <div class="hide" id="<?= $member['user_nicename']; ?>-content">
        <?= $user_bio; ?>
    </div>
</div>