<!-- Carousel -->
<div id="carousel1" class="carousel slide cyan-800-bg" data-ride="carousel">
    <?php if( have_rows('carousel') ): ?>
        <div class="carousel-inner" role="listbox">
            <?php
            $counter = 1;
            while (have_rows('carousel')): the_row();
                $thePost = get_sub_field('post');
                $categories = get_the_category( $thePost->ID );
                $author = get_userdata($thePost->post_author)->data;
                ?>
                <div class="item<?= ( $counter == 1 ) ? ' active' : '' ?>">
                    <div class="hidden-xs col-sm-6">
                        <img src="<?= wp_get_attachment_url( get_post_thumbnail_id($thePost->ID) ); ?>" class="img-responsive"/>
                        <a class="highlight post-category cyan-a100" href="/category/<?= $categories[0]->slug; ?>"><?= $categories[0]->name; ?></a>
                        <a class="post-meta cyan-a100" href="/author/<?= $author->user_nicename; ?>"><p class="post-meta"><i class="fa fa-user"></i> <?= $author->display_name; ?> : <?= mysql2date('d.m.Y', $thePost->post_date); ?></p></a>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <div class="carousel-caption">
                            <h4><?= $thePost->post_title; ?></h4>
                            <p class="carousel-description hidden-xs"><?= truncateText(strip_tags($thePost->post_content), 350); ?></p>
                            <p class="carousel-description visible-xs"><?= truncateText(strip_tags($thePost->post_content), 250); ?></p>
                            <input type="hidden" name="carousel-c-link" value="<?= get_post_permalink($thePost->ID); ?>" />
                        </div>
                    </div>
                </div>
                <?php
                $counter++;
            endwhile;
            ?>
        </div>

        <div class="carousel-bottom">
            <div class="col-xs-12 col-sm-6">
            </div>
            <div class="col-xs-12 col-sm-6">
                <div class="col-sm-8 hidden-xs">
                    <div class="row">
                        <button class="btn cyan-500-bg" onclick="gotoCarouselLink('carousel1')"><i class="fa fa-chevron-right"></i> Read more</button>
                    </div>
                </div>
                <div class="col-xs-offset-2 col-xs-4 col-sm-offset-0 col-sm-4">
                    <div class="carousel-controls">
                        <a class="left carousel-control" href="#carousel1" role="button" data-slide="prev">
                            <span class="fa fa-chevron-left" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>

                        <a class="right carousel-control" href="#carousel1" role="button" data-slide="next">
                            <span class="fa fa-chevron-right" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>

</div>
<!-- /.carousel -->