<div class="col-xs-12 col-md-<?= $colMd; ?>">
    <div class="education-post <?= $blogEntryColor; ?>">
        <a href="<?= get_post_permalink($post->ID); ?>">
            <img src="<?= wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>" class="img-responsive"/>
        </a>
        <?php
        $post = get_post();
        if ( $post ) {
            $categories = get_the_category( $post->ID );
            $author = get_userdata($post->post_author)->data;
        }
        ?>
        <h2 class="post-title">
            <a class="post-link" href="<?= the_permalink(); ?>"><?= the_title(); ?></a>
        </h2>

        <p class="post-excerpt"><?= truncateText(strip_tags($post->post_content), 350); ?></p>
        <div class="post-actions">
            <p class="post-meta">
                <i class="fa fa-tags"></i>
                <a class="post-category" href="/category/<?= $categories[0]->slug; ?>"><?= $categories[0]->name; ?></a>
            </p>
            <p class="post-meta">
                <i class="fa fa-paint-brush"></i>
                <a class="post-author" href="/blog/author/<?= $author->user_nicename; ?>"><?= $author->display_name; ?></a> : <?= mysql2date('d.m.Y', $post->post_date); ?>
            </p>
            <p class="post-meta">
                <a class="read-more bottom-right" href="<?= the_permalink(); ?>">Read more <i class="fa fa-hand-o-right"></i></a>
            </p>
        </div>
    </div>
</div>
