<div class="row">
    <div class="col-md-12">
        <?php if( ! empty($title) ): ?>
        <h2 class="indigo-900">
            <?php echo $title; ?>
            <?php if ( ! empty($subtitle)): ?>
                <span class="subtitle"><?php echo $subtitle; ?></span>
            <?php endif; ?>
        </h2>
        <?php endif; ?>
        <?php echo $body; ?>
    </div>
</div>