<?php
/**
 * Default Template
 */
?>
<?php get_header() ?>

    <div id="primary" class="boxed-layout-header page-header header-small">
        <?php template_header_background( false ); ?>
    </div>
</header>
<div class="main main-raised">
    <div class="blog-post">
        <div class="container">
            <?php
                if ( have_posts() ) :
                    while ( have_posts() ) :
                        the_post();
                        get_template_part( 'includes/content', 'page' );
                    endwhile;
                else :
                    get_template_part( 'includes/content', 'none' );
                endif;
            ?>
        </div>
    </div>
    <?php get_footer(); ?>
