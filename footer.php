<!-- Footer -->
    <section>
        <footer class="footer footer-black footer-big">
            <div class="container">
                <div class="hestia-bottom-footer-content">
                    <div class="hestia-bottom-footer-content">
                        <?php wp_nav_menu( array('menu' => 'Footer menu', 'container' => '', 'menu_class' => 'footer-menu pull-left', 'menu_id' => 'menu-footer-menu' )); ?>

<!--                        <ul id="menu-footer-menu" class="footer-menu pull-left">-->
<!--                            <li id="menu-item-35" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-32 current_page_item menu-item-35"><a href="http://192.168.0.31/">Domov</a></li>-->
<!--                            <li id="menu-item-36" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-36"><a href="http://192.168.0.31/piskotki/">Piškotki</a></li>-->
<!--                        </ul>-->
                        <div class="copyright pull-right">
                            <a href="http://jslejko.com" target="_blank" rel="nofollow">Jernej Slejko</a> | &copy;2018
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </section>
<?php wp_footer(); ?>
</body>
</html>