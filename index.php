<?php get_header() ?>

<div id="primary" class="boxed-layout-header page-header header-small">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1 text-center">
                <h1 class="hestia-title"><?php is_front_page() ? bloginfo( 'description' ) : wp_title( '' ); ?></h1>
            </div>
        </div>
    </div>
    <?php template_header_background( false ); ?>
</div>
</header>
<div class="main main-raised">
    <div class="hestia-blogs">
        <div class="container">
            <div class="row">
                <div class="<?php echo esc_attr( $class_to_add ); ?>">
                    <?php if ( have_posts() ) : ?>
                        <article id="post-<?php the_ID(); ?>" class="section section-text">
                            <div class="row">
                                <div class="col-md-12">
                                    <?php the_content(); ?>
                                </div>
                            </div>
                        </article>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>

    <?php get_footer(); ?>
