<?php
    $my_fields = get_field_object('features');
    $count = (count($my_fields['value']));
    $divider = floor(12 / $count);
?>
<section class="templ-features" id="features">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <h5 class="description">
                    <?= get_field('features_description'); ?>
                </h5>
            </div>
        </div>
        <div class="templ-features-content">
            <div class="row">
                <?php while (have_rows('features')): the_row(); ?>
                <div class="col-xs-12 col-md-<?= $divider; ?> feature-box">

                    <div class="templ-info">
                        <div class="icon" style="color:#de4b39"><i class="fa fa-<?php echo get_sub_field('feature')->icon; ?>"></i></div>
                        <h4 class="info-title"><?php echo get_sub_field('feature')->title; ?></h4>
                        <p>
                            <?php echo get_sub_field('feature')->description; ?>
                        </p>
                    </div>
                </div>
                <?php endwhile; ?>
            </div>
        </div>
    </div>
</section>
