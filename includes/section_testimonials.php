<?php
    $my_fields = get_field_object('testimonials');
    $count = (count($my_fields['value']));
    $divider = floor(12 / $count);
    $counter = 0;
?>
<section class="templ-testimonials" id="testimonials">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2 text-center">
                <h2 class="templ-title"><?= get_field('testimonial_title'); ?></h2>
                <h5 class="description"><?= get_field('testimonial_subtitle'); ?></h5>
            </div>
        </div>
        <div class="templ-testimonials-content">
            <div class="row">
                <?php while (have_rows('testimonials')): the_row(); ?>
                <?php $counter++; ?>
                <div class="col-xs-12 col-ms-6 col-sm-6 col-md-<?= $divider; ?>">
                    <div class="card card-testimonial card-plain">
                        <div class="card-avatar">
                            <img class="img" src="/wp-content/themes/wp-theme-1/images/<?= $counter; ?>.jpg" alt="<?= get_sub_field('testimonial_author'); ?>" title="<?= get_sub_field('testimonial_author'); ?>">
                        </div>
                        <div class="content">
                            <h4 class="card-title"><?= get_sub_field('testimonial_author'); ?></h4>
                            <h6 class="category text-muted"><?= get_sub_field('testimonial_author_city'); ?></h6>
                            <p class="card-description">
                                <q><?= get_sub_field('testimonial_quote'); ?></q>
                            </p>
                        </div>
                    </div>
                </div>
                <?php endwhile; ?>
            </div>
        </div>
    </div>
</section>
