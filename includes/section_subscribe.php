<section class="subscribe-line subscribe-line-image" id="subscribe"
         style="background-image: url('<?= get_field('subscribe_image'); ?>');">
    <div class="container">
        <div class="row text-center">
            <div class="col-md-8 col-md-offset-2 text-center">
                <h2 class="title"><?= get_field('subscribe_title'); ?></h2>
                <h5 class="subscribe-description"><?= get_field('subscribe_subtitle'); ?></h5>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="card card-raised card-form-horizontal">
                    <div class="content">
                        <div class="row">
                            form from somewhere
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
